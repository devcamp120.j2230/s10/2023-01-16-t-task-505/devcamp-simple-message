//Khai báo thư viện express
const express = require("express");

//Khởi tạo app nodejs
const app = express();

//Khai báo cổng để chạy ứng dụng
const port = 8000;

//khai báo middelware để làm việc với json của request body
app.use(express.json());

//*********Task 505.10**********

//Khai báo api / trả về 1 message ngày tháng
app.get('/', (request, response) => {
    var date = new Date();
    console.log(date.getTime());

    response.status(200).json({
        message : `Hôm nay là ngày ${date.getDate()} tháng ${date.getMonth()} năm ${date.getFullYear()}`
    })
})

//*********Task 505.20**********
//Khai báo phương thức get
app.get('/get-method', (req, res) => {
    console.log("Get Method");
    res.status(200).json({
        "message" : "Get Method"
    })
})

//Khai báo phương thức post
app.post('/post-method', (req, res) => {
    console.log("Post Method");
    res.status(200).json({
        "message" : "Post Method"
    })
})

//Khai báo phương thức put
app.put('/put-method', (req, res) => {
    console.log("Put Method");
    res.status(200).json({
        "message" : "Put Method"
    })
})

//Khai báo phương thức delete
app.delete('/delete-method', (req, res) => {
    console.log("Delete Method");
    res.status(200).json({
        "message" : "Delete Method"
    })
})

//*********Task 505.30**********
//tham số trên đường dẫn
app.get('/get-param/:param1/:param2/:param3', (req, res) => {
    var param1 = req.params.param1;
    var param2 = req.params.param2;
    var param3 = req.params.param3;

    console.log("Get Method");
    res.status(200).json({
        "message" : "Get Param",
        param1,
        param2,
        param3
    })
})

//tham số querystring: /get-query?param1=123&param2=abc
app.get('/get-query', (req, res) => {
    var query = req.query;
    console.log('Get Query');
    console.log(query);
    res.status(200).json({
        "message":"Get Query",
        query
    })
})

//tham số request body json
app.get('/get-body', (req, res) => {
    var body = req.body;
    console.log('Get Body');
    console.log(body);
    res.status(200).json({
        "message":"Get Body",
        body
    })    
})
//Mở ứng dụng trên cổng 8000
app.listen(port, () => {
    console.log(`App running on port ${port}`);
})